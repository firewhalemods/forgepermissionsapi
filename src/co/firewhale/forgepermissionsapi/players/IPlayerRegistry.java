package co.firewhale.forgepermissionsapi.players;

import net.minecraft.entity.player.EntityPlayer;

public interface IPlayerRegistry {
    /**
     * @param username
     * @return user id of a username
     */
    int getID(String username);

    /**
     * @param player
     * @return user id of a player entity
     */
    int getID(EntityPlayer player);

    /**
     * @param id
     * @return Username of an ID
     */
    String getUsername(int id);
}
