package co.firewhale.forgepermissionsapi;

import co.firewhale.forgepermissionsapi.players.IPlayerRegistry;
import net.minecraft.entity.player.EntityPlayer;

public class PermissionsAPI {
    private static IPermissionsAPI api = null;

    /**
     * API Entry Point
     * @return the {@link IPermissionsAPI}
     */
    public static IPermissionsAPI instance() {
        if (api == null) {
            try {
                Class c = Class.forName("co.firewhale.forgepermissions.core.api");
                api = (IPermissionsAPI)c.getField("instance").get(c);
            } catch (Throwable ex) {
                return null;
            }
        }

        return api;
    }


    public void test() {
        boolean hasPermission = api.permissionsRegistry().hasPermission("FireBall1725", "com.fireball1725.commands.kill.shamu");
    }

}
