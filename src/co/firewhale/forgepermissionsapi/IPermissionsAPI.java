package co.firewhale.forgepermissionsapi;

import co.firewhale.forgepermissionsapi.permissions.IPermissionsRegistry;
import co.firewhale.forgepermissionsapi.players.IPlayerRegistry;

public interface IPermissionsAPI {
    /**
     * @return Player Registry
     */
    IPlayerRegistry playerRegistry();

    /**
     * @return Permissions Registry
     */
    IPermissionsRegistry permissionsRegistry();
}
