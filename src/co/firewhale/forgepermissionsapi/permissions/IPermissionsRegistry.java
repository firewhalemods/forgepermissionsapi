package co.firewhale.forgepermissionsapi.permissions;

import net.minecraft.entity.player.EntityPlayer;

public interface IPermissionsRegistry {
    /**
     * Get if player has permission
     * @param player PlayerEntity to check
     * @param Permission Permission Node
     * @return Permissions
     */
    boolean hasPermission(EntityPlayer player, String Permission);

    /**
     * Get if player has permission
     * @param playerName Players Name to check
     * @param Permission Permission Node
     * @return Permissions
     */
    boolean hasPermission(String playerName, String Permission);

    /**
     * Get if player has permissions
     * @param playerID Player ID to check
     * @param Permission Permission Node
     * @return Permissions
     */
    boolean hasPermission(int playerID, String Permission);

    // Idea to get permission name from class name?
    // Code would do something like
    /*
        String Permission = c.getName();
        which should return something like "my.full.dotted.classname.MyClass"
     */
    boolean hasPermission(EntityPlayer player, Class<?> c);
}
